provider "aws" {
  region = "us-east-1"
}

resource "aws_ecr_repository" "cloud_assignment_repo" {
  name = "cloud-assignment"
}

resource "aws_ecr_repository_policy" "cloud_assignment_repo_policy" {
  repository = aws_ecr_repository.cloud_assignment_repo.name

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect    = "Allow"
        Principal = "*"
        Action    = [
          "ecr:GetDownloadUrlForLayer",
          "ecr:BatchGetImage",
          "ecr:BatchCheckLayerAvailability",
          "ecr:PutImage",
          "ecr:InitiateLayerUpload",
          "ecr:UploadLayerPart",
          "ecr:CompleteLayerUpload"
        ]
      }
    ]
  })
}

resource "docker_registry_image" "cloud_assignment_image" {
  name = "${aws_ecr_repository.cloud_assignment_repo.repository_url}:${var.image_tag}"

  build {
    context    = "../aspnet-core-dotnet-core"
    dockerfile = "../aspnet-core-dotnet-core/Dockerfile"
  }
}

variable "image_tag" {
  type    = string
  default = "latest"
}
