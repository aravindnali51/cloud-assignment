# Use the official ASP.NET Core runtime as a parent image
FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80

# Use the SDK image to build the app
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY aspnet-core-dotnet-core/aspnet-core-dotnet-core.csproj ./
RUN dotnet restore "./aspnet-core-dotnet-core.csproj"
COPY aspnet-core-dotnet-core/ .
RUN dotnet build "./aspnet-core-dotnet-core.csproj" -c Release -o /app/build

# Publish the app to the /app/publish directory
FROM build AS publish
RUN dotnet publish "./aspnet-core-dotnet-core.csproj" -c Release -o /app/publish

# Copy the published app to the base image
FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "aspnet-core-dotnet-core.dll"]
